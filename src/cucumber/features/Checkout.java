package cucumber.features;

public class Checkout {
	
	private int runningTotal=0; 
	
	// only the class in which it is declared can see it 
	
	public void add(int count, int price)
	{
		this.runningTotal+=count*price;
		
	}
	
	public int total()
	{
		return this.runningTotal;
	}

}
