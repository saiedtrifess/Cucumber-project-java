package cucumber.features;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.junit.Cucumber;
import static org.junit.Assert.*;

public class StepDefinitions {
	int bananaPrice =0;
	Checkout checkout = new Checkout();
	
	
	@Given("^the price of a \"([^\"]*)\" is (\\d+)c$")
	public void thePriceOfAIsc(String name, int price) throws Throwable {
	     bananaPrice=price;
	    
		//throw new PendingException();
	    //Step was found but step hasn't been implemented 
	}

	@When("^I checkout (\\d+) \"([^\"]*)\"$")
	public void i_checkout(int itemCount, String itemName) throws Throwable {
	    checkout.add(itemCount, bananaPrice);
	}

	@Then("^the total price, should be (\\d+)c$")
	public void theTotalPriceShouldBeC ( int total) throws Throwable {
		  assertEquals(total, checkout.total());
		  }
	
	 
	
	}

// ubiquitousي
